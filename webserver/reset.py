# Sophie Johnson
# 11/24/18

import cherrypy
import json
from _song_database import _song_database

# Reset controller class
class ResetController(object):

    # Constructor
    def __init__(self, sdb=None):
        if sdb is None:
            self.sdb = _song_database()
        else:
            self.sdb = sdb

    # Reset entire database
    def PUT_INDEX(self):
        output = {'result' : 'success'}
        try:
            self.sdb.reset_database()
            self.sdb.load_database_info('songData.json')
        # Handle exception as error
        except Exception as e:
            output['result'] = 'error'
            output['message'] = str(e)
        return json.dumps(output)

    # Reset a single song 
    def PUT_SONG(self, song):
        output = {'result' : 'success'}
        song = str(song)
        # Attempt to load song data then reset
        try:
            sdb_tmp = _song_database()
            sdb_tmp.load_database_info('songData.json')
            info = sdb_tmp.get_song_info(song)
            if info:
                song_dict = {}
                song_dict['song'] = info[0]
                song_dict['year'] = info[1]
                song_dict['artist'] = info[2]
                song_dict['genre'] = info[3]
                song_dict['lyrics'] = info[4]
                self.sdb.delete_song(song)
                self.sdb.add_song(json.dumps(song_dict))
            else:
                output['result'] = 'error'
                output['message'] = "Song not found in database." 
        # Handle exception as error
        except Exception as e:
            output['result'] = 'error'
            output['message'] = str(e)
        return json.dumps(output)
